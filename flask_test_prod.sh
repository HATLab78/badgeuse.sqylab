cd /home/sqylab/badgeuse.sqylab/
export FLASK_APP=src
# export FLASK_ENV=development
export FLASK_ENV=production
source ./venv/bin/activate
flask run --port 80
# pprofile --exclude-syspath -m flask -- run
# python -m cProfile -s calls src/__init__.py
# [-o output_file]
# python -m cProfile -s calls src/run.py
