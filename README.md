# README

## Installation

Voir [INSTALL.md](INSTALL.md).

## En cas de bugs

### L'application ne se lance pas

Redémarrer l'orangepi. Si ça ne marche toujours pas contacter le bureau.

### Le système est lancé mais Firefox est fermé ou l'onglet badgeuse est fermé

Redémarrer Firefox, le fermer et l'ouvrir.

### Il y a un bug dans l'application

Le rapporter sous le menu `bug`.

### La touche "@" ne marche pas

C'est un problème de keyboard layout. Ouvrir un terminal et y taper: `sudo dpkg-reconfigure keyboard-configuration`. Suivre les instructions. Choisir touche `alt-gr` = `alt droite`.

## Lancement du service au démarrage:

Ajout de Firefox au programme qui se lance au démarrage:

* Menu démarrer -> accessoire -> Trouver un programme -> taper `startup` dans la barre de recherche
* ajouter un programme -> nom: `Firefox` -> execute: `firefox`

Ajout d'un cron job pour lancer la badgeuse au démarrage:

```
sudo crontab -e
```

Ajouter les lignes suivante au fichier:

```
# Lancement de l'application badgeuse au démarrage de l'orangepi
@reboot /home/sqylab/Projet-RFID/start-jobs.sh
```
Attention si changement du nom d'utilisateur il faudra éditer le chemin.

## Paramétrage du port SPI

# Architecture - Fonctionnement général

A decrire

# Montage du répertoire distant Nextcloud

```
sudo aptitude install davfs2
sudo dpkg-reconfigure davfs2 # configure qui a accès
sudo mkdir /mnt/badgeuse
sudo mount -t davfs -o noexec https://nextcloud.hatlab.fr/remote.php/webdav /mnt/badgeuse/
sudo vim /mnt/badgeuse/test.txt # test l'écriture sur le répertoire distant
```
