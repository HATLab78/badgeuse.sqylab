#!/bin/bash
cd $HOME/
badgeuse=$(find ./ -maxdepth 3 -name badgeuse.sqylab 2>/dev/null)
cd $badgeuse
echo $(pwd)
export FLASK_APP=src
if [ "$1" = "development" ]
then
  export FLASK_ENV=$1
  if [ -f "test-data/admin.db" ]; then
    echo "database exists skipping database creation"
  else
    echo "database doesn't exist creating database"
    flask init-db
  fi
elif [ "$1" = "production" ]; then
  export FLASK_ENV=production
  if [ -f "data/admin.db" ]; then
    echo "database exists skipping database creation"
  else
    echo "database doesn't exist creating database"
    flask init-db
  fi
else
  echo "Please start the app in either production or development mode"
  echo "For instance with : './start.sh development'"
  exit 1
fi
echo "Mode: $FLASK_ENV"
source ./venv/bin/activate
echo "VirtualEnv Activated!"
flask run
# pprofile --exclude-syspath -m flask -- run
# python -m cProfile -s calls src/__init__.py
# [-o output_file]
# python -m cProfile -s calls src/run.py
