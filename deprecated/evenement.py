from flask import Blueprint, flash, render_template, request

from .tools.Constants import Constants
from .tools.logger import log_request
from .tools.Manager import Manager

c = Constants()

bp = Blueprint('evenement', __name__, url_prefix='/evenement')
manager = Manager()


@bp.route(c.APP_PATHS[c.APP_ROOT], methods=[c.WEB_GET, c.WEB_POST])
def retourner_evenement():
    """Enregistre un événement."""
    log_request(c.APP_EVENEMENT, request)
    kwargs = {c.WEB_ACTIVE: c.APP_EVENEMENT}
    kwargs.update(c.APP_PATHS)

    if request.method == c.WEB_POST and c.PARTICIPANTS not in request.form.keys():
        print(request.form)
        flash("Événement enregistré! Merci d'animer le FABLAB!")
        manager.ajouter_evenement(request.form)
    if request.method == c.WEB_POST and c.PARTICIPANTS in request.form.keys():
        manager.editer_evenement(request.form)

    DIX = 10
    contenu = manager.obtenir_derniers_evenements(DIX)
    # logging.info("{} derniers évènements: {}".format(DIX, contenu))
    kwargs[c.WEB_CONTENU] = contenu

    return render_template("{}.html".format(c.APP_EVENEMENT), **kwargs)
