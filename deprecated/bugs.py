from flask import (Blueprint, flash, get_url, redirect, render_template,
                   request, url_for)
from flask_security import login_required
from markdown import markdown

from .tools.Constants import Constants
from .tools.logger import log_request
from .tools.Manager import Manager

c = Constants()

bp = Blueprint('auth', __name__, url_prefix='/auth')
manager = Manager()


@bp.route(c.APP_PATHS[c.APP_BUG], methods=[c.WEB_GET, c.WEB_POST])
def retourner_bug():
    """Telegram a bug."""
    log_request(c.APP_BUG, request)

    if request.method == c.WEB_POST and request.form[c.WEB_BOUTON] == c.WEB_ENVOYER:
        ligneCsv = {c.NOM: request.form[c.NOM], c.PRENOM: request.form[c.PRENOM],
                    c.DESCRIPTION: request.form[c.WEB_TEXTE]}
        manager.ajouter_bug(ligneCsv)
        msg = "Bug rapporté par {} {}\n{}".format(ligneCsv[c.PRENOM], ligneCsv[c.NOM], ligneCsv[c.DESCRIPTION])
        c.TELEGRAM_API_MESSAGE_PAYLOAD["text"] = msg
        r = get_url(c.TELEGRAM_API_URL, params=c.TELEGRAM_API_MESSAGE_PAYLOAD)
        if r.status_code == 200:
            flash("Telegram envoyé avec succès!")
        else:
            flash("Telegram non envoyé!")
        return redirect(url_for("get_accueil"))

    return render_template("{}.html".format(c.APP_BUG), active=c.APP_BUG, **c.APP_PATHS)


@bp.route(c.APP_PATHS[c.APP_BUGS])
@login_required
def retourner_bugs():
    log_request(c.APP_BUGS, request)

    kwargs = {c.WEB_ACTIVE: c.APP_BUGS, c.WEB_CONTENU: manager.obtenir_bugs()}
    kwargs.update(c.APP_PATHS)

    return render_template("{}.html".format(c.APP_BUGS), **kwargs)


@bp.route(c.APP_PATHS[c.APP_CHANGELOG])
def retourner_changelog():
    """Affiche les derniers changement sur le logiciel."""
    log_request(c.APP_CHANGELOG, request)
    with open(c.CHEMIN_CHANGELOG, mode="r") as changelog:
        html = markdown(changelog.read())

    kwargs = {c.WEB_CONTENU: html, c.WEB_ACTIVE: c.APP_CHANGELOG}
    kwargs.update(c.APP_PATHS)
    return render_template("{}.html".format(c.APP_CHANGELOG), **kwargs)
