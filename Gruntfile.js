module.exports = function(grunt) {
    grunt.initConfig({
        dirs: {
            css: 'src/raw/css',
            static: 'src/static'
        },
        watch: {
            options: {
                spawn: false
            },
            static: {
                files: '<%= dirs.css %>/**/*',
                tasks: ['uncss']
            },
        },
        uncss: {
            dist: {
                files: [{
                    nonull: true,
                    src: ['http://localhost:5000/accueil', 'http://localhost:5000/admin', 'http://localhost:5000/historique', 'http://localhost:5000/login', 'http://localhost:5000'],
                    dest: '<%= dirs.static %>/css/tidy.css'
                }]
            }
        },
        bgShell: {
          flask: {
            cmd: "bash ./start.sh development && echo 'done'",
            bg: true
          },
          wait: {
            cmd: "sleep 2",
            bg: false
          }
        }
    });

    // load npm tasks
    grunt.loadNpmTasks('grunt-bg-shell');
    grunt.loadNpmTasks('grunt-uncss');
    grunt.loadNpmTasks('grunt-contrib-watch');
    // define default task
    grunt.registerTask('default', ['bgShell:flask', 'bgShell:wait', 'uncss', 'watch']);
};
