from datetime import date, datetime, timedelta

from flask import Blueprint
from flask import current_app as app
from flask import redirect, render_template, request, url_for

from .tools.logger import log_request
from .config import CONSTANTS as c


bp = Blueprint('historique', __name__, url_prefix='/historique')
urlHistorique = "historique.retourner_historique"


@bp.route(c.APP_PATHS[c.APP_ROOT], methods=[c.WEB_GET])
def retourner_historique_du_jour():
    log_request(c.APP_HISTORIQUE, request)
    ceJour = datetime.today()
    return redirect(url_for(urlHistorique, annee=ceJour.year, mois=ceJour.month, jour=ceJour.day))


@bp.route(c.APP_PATHS[c.APP_ROOT] + "/<annee>/<mois>/<jour>", methods=[c.WEB_GET])
def retourner_historique(annee, mois, jour):
    log_request(c.APP_HISTORIQUE, request)
    kwargs = {}
    kwargs[c.WEB_ACTIVE] = c.APP_HISTORIQUE
    kwargs.update(c.APP_PATHS)
    ceJour = datetime.today()
    kwargs["ceJour"] = url_for(urlHistorique, annee=ceJour.year, mois=ceJour.month, jour=ceJour.day)
    jourSuivant = date(year=int(annee), month=int(mois), day=int(jour)) + timedelta(+1)
    kwargs["suivant"] = url_for(urlHistorique, annee=jourSuivant.year, mois=jourSuivant.month, jour=jourSuivant.day)
    jourPrecedent = jourSuivant + timedelta(-2)
    kwargs["precedent"] = url_for(urlHistorique, annee=jourPrecedent.year, mois=jourPrecedent.month, jour=jourPrecedent.day)
    print("Historique - retourner_historique: {}".format(date(year=int(annee), month=int(mois), day=int(jour))))
    entreesDuJour = app.manager.rechercher_date(date(year=int(annee), month=int(mois), day=int(jour)))
    kwargs[c.WEB_ENTREES] = app.manager.colorer_entrees_avec_fin_cotisation(entreesDuJour)
    kwargs["date"] = "{}-{}-{}".format(jour, mois, annee)

    return render_template("{}.html".format(c.APP_HISTORIQUE), **kwargs)


# Implement lookup of adherent visits
# @bp.route("/" + c.WEB_AJOUTER, methods=[c.WEB_GET])
# def ajouter():
#     return redirect(url_for("accueil.simuler") + "?index=" + request.args[c.WEB_ADHERENT_INDEX])


@bp.route(c.APP_PATHS[c.APP_RECHERCHE], methods=[c.WEB_POST])
def retourner_date():
    if request.form[c.WEB_BOUTON] == c.WEB_RECHERCHER:
        jour = request.form[c.WEB_JOUR]
        mois = request.form[c.WEB_MOIS]
        annee = request.form[c.WEB_ANNEE]
        return redirect(url_for(urlHistorique, annee=annee, mois=mois, jour=jour))
    else:
        return redirect(url_for(urlHistorique))
