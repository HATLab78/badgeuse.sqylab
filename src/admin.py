import functools
from datetime import timedelta
from os import path

from flask import Blueprint
from flask import current_app as app
from flask import (flash, g, redirect, render_template, request, session,
                   url_for)
from werkzeug.security import check_password_hash, generate_password_hash

from .db import get_db
from .tools.logger import log_request
from .config import CONSTANTS as c


bp = Blueprint('admin', __name__, url_prefix='/admin')


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('admin.login'))

        return view(**kwargs)

    return wrapped_view


@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = get_db().execute(
            'SELECT * FROM user WHERE id = ?', (user_id,)
        ).fetchone()


@bp.before_request
def make_session_permanent():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=5)


@bp.route(c.APP_PATHS[c.APP_REGISTER], methods=('GET', 'POST'))
@login_required
def register():
    kwargs = {c.WEB_ACTIVE: c.APP_REGISTER}
    kwargs.update(c.APP_PATHS)
    kwargs.update(request.form)

    if request.method == 'POST':
        username = request.form[c.EMAIL]
        password = request.form[c.PASSWORD]
        db = get_db()
        error = None

        if not username:
            error = 'Username is required.'
        elif not password:
            error = 'Password is required.'
        elif db.execute(
            'SELECT id FROM user WHERE username = ?', (username,)
        ).fetchone() is not None:
            error = 'User {} is already registered.'.format(username)

        if error is None:
            db.execute('INSERT INTO user (username, password) VALUES (?, ?)',
                       (username, generate_password_hash(password)))
            db.commit()
            return redirect(url_for('admin.login'))

        flash(error)

    return render_template('register.html', **kwargs)


@bp.route(c.APP_PATHS[c.APP_LOGIN], methods=('GET', 'POST'))
def login():
    kwargs = {c.WEB_ACTIVE: c.APP_ADMIN}
    kwargs.update(c.APP_PATHS)
    kwargs.update(request.form)
    if request.method == 'POST':
        username = request.form[c.EMAIL_TYPE]
        password = request.form[c.PASSWORD]
        db = get_db()
        error = None
        user = db.execute('SELECT * FROM user WHERE username = ?', (username,)).fetchone()

        if user is None:
            error = 'Incorrect username.'
        elif not check_password_hash(user['password'], password):
            error = 'Incorrect password.'

        if error is None:
            session.clear()
            session['user_id'] = user['id']
            return redirect(url_for('admin.retourner_admin'))

        flash(error)

    return render_template("login.html", **kwargs)


@bp.route('/logout')
def logout_manuel():
    session.clear()
    return redirect("/")


@bp.route(c.APP_PATHS[c.APP_RECHERCHE], methods=[c.WEB_POST])
@login_required
def rechercher_adherent_pour_rfid():
    log_request(c.APP_ADMIN + c.WEB_RECHERCHER, request)
    session[c.RFID_NON_ATTRIBUE] = app.manager.dernierRfid
    print("admin.rechercher_entrees")

    kwargs = {c.WEB_ACTIVE: c.APP_ADMIN}
    kwargs.update(c.APP_PATHS)
    kwargs.update(request.form)
    session[c.WEB_ADHERENTS] = app.manager.rechercher_adherent(request.form[c.NOM])
    for item in session[c.WEB_ADHERENTS]:
        item[c.URI] = item[c.URI].format(URI=c.WEB_AJOUTER)

    kwargs[c.WEB_ADHERENT_INDEX] = list(range(len(session[c.WEB_ADHERENTS])))
    kwargs[c.WEB_CONTENU] = session[c.WEB_ADHERENTS]

    return render_template("{}.html".format(c.APP_ADMIN), **kwargs)


@bp.route(c.APP_PATHS[c.APP_ROOT] + c.WEB_ENTREE, methods=[c.WEB_GET])
@login_required
def rechercher_entrees():
    log_request(c.APP_ADMIN + c.WEB_ENTREE, request)

    kwargs = {c.WEB_ACTIVE: c.APP_HISTORIQUE}
    resultatsNom = app.manager.rechercher_entrees(request.args[c.NOM])
    resultats = [r for r in resultatsNom if r[c.PRENOM] == request.args[c.PRENOM]]
    kwargs.update({c.NOM: request.args[c.NOM], c.WEB_CONTENU: resultats})

    return render_template("{}.html".format(c.APP_HISTORIQUE), **kwargs)


@bp.route(c.APP_PATHS[c.APP_ROOT] + c.WEB_AJOUTER, methods=[c.WEB_GET])
@login_required
def ajouter_rfid():
    """Associer un numero de badge c.RFID a un adherent"""
    log_request(c.APP_ADMIN + c.WEB_AJOUTER, request)
    session[c.WEB_ADHERENT] = session[c.WEB_ADHERENTS][int(request.args[c.WEB_ADHERENT_INDEX])]
    reponse = app.manager.ajouter_rfid_adherent(session[c.WEB_ADHERENT], session[c.RFID_NON_ATTRIBUE])
    flash(reponse)

    return redirect(url_for("admin.retourner_admin"))


@bp.route(c.APP_PATHS[c.APP_ROOT] + c.WEB_SUPPRIMER, methods=[c.WEB_POST])
@login_required
def supprimer_rfid():
    log_request(c.APP_ADMIN + c.WEB_SUPPRIMER, request)

    numero = request.form[c.WEB_NUMERO]
    texte = app.manager.supprimer_rfid_adherent(numero)
    flash(texte)
    return redirect(url_for("admin.retourner_admin"))


@bp.route(c.APP_PATHS[c.APP_ROOT], methods=[c.WEB_POST, c.WEB_GET])
@login_required
def retourner_admin():
    log_request(c.APP_ADMIN, request)
    kwargs = {c.WEB_ACTIVE: c.APP_ADMIN}
    kwargs.update(c.APP_PATHS)
    session[c.RFID_NON_ATTRIBUE] = app.manager.dernierRfid
    return render_template("{}.html".format(c.APP_ADMIN), **kwargs)


# NOTE: DEPRECATED
# @bp.route(c.APP_PATHS[c.APP_ROOT], methods=[c.WEB_POST])
# @login_required
# def mettre_a_jour_adherents():
#     if request.method == c.WEB_POST and request.form[c.WEB_BOUTON] == c.WEB_TELEVERSER:
#         if "file" not in request.files:  # check if the post request has the file part
#             flash("Pas de fichier...")
#             return redirect(request.url)
#         fichier = request.files["file"]
#         if fichier.filename == "":  # if user does not select file, browser also submit an empty part without filename
#             flash("Aucun fichier séléctionné")
#             return redirect(request.url)
#
#         cheminFichier = path.join(c.UPLOAD_FOLDER, fichier.filename)
#
#         resultat = app.manager.mise_a_jour_adherents(fichier, cheminFichier)
#         if resultat is True:
#             flash("Fichier: {} téléversé!".format(resultat))
#         else:
#             flash("Erreur, fichier non compatible...")
#             for text in resultat.split("\n"):
#                 flash(text)
