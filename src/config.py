from .Constants import Constants
from os import environ


if environ.get("FLASK_ENV") == "development":
    from .app_secrets_test import SECRET_KEY, SECURITY_PASSWORD_SALT, NEXTCLOUD_PASSWORD, ADMIN, ADMIN_PASSWORD
    CONSTANTS = Constants(dataPath="test-data")
else:
    from .app_secrets import SECRET_KEY, SECURITY_PASSWORD_SALT, NEXTCLOUD_PASSWORD, ADMIN, ADMIN_PASSWORD
    CONSTANTS = Constants()
# NOM = CONSTANTS.NOM

APP_ADMIN = CONSTANTS.APP_ADMIN
UPLOAD_FOLDER = CONSTANTS.UPLOAD_FOLDER

SECURITY_POST_LOGIN_VIEW = APP_ADMIN
SECURITY_LOGIN_USER_TEMPLATE = "login_user.html"
WEBDAV_UPLOAD = False
