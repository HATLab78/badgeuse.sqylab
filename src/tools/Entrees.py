import logging
from datetime import timedelta

from .ObjetCSV import ObjetCSV
from .utils.Profiler import profile
from .utils.Validate import Validate


logging.basicConfig(level=logging.INFO)


class Entrees(ObjetCSV):
    """Entrees permet de gérer le fichier adhérents et de le tester."""
    def __init__(self, *args, **kwargs):
        super(Entrees, self).__init__(*args, **kwargs)
        # self.dernieresEntrees = Queue(maxsize=1)
        # self.chargerDernieresEntrees = Thread(target=self.charger_fichier, args=(self.rechercher_entrees_du_jour))
        # self.chargerHistorique = Thread(target=self.charger_fichier, args=(self.rechercher_entrees_du_jour, jour))
        self.update_stats()
        self.update_unique_stats()

    def validate_csv(self):
        try:
            self.validate_entetes_csv()
        except Exception as e:
            logging.critical("Fichier CSV avec des entêtes non valide: {}".format(e))

    def validate_entetes_csv(self):
        assert self.c.NOM in self.fieldnames, "Entête manquante: {}".format(self.c.NOM)
        assert self.c.PRENOM in self.fieldnames, "Entête manquante: {}".format(self.c.PRENOM)
        assert self.c.DATE in self.fieldnames, "Entête manquante: {}".format(self.c.DATE)
        assert self.c.HEURE in self.fieldnames, "Entête manquante: {}".format(self.c.HEURE)
        logging.info("Entêtes csv OK.")

    def validate_date(self):
        for ligne in self.csvDict():
            dateValide = Validate()
            dateValide.tester_date(ligne[self.c.DATE])
        logging.info("Les dates sont OK.")

    def validate_heure(self):
        for ligne in self.csvDict():
            dateValide = Validate()
            dateValide.tester_date(ligne[self.c.DATE])
        logging.info("Les heures sont OK.")

    def rechercher_nom_et_prenom(self, nom, prenom):
        resultatNom = self.rechercher_nom(nom)
        return self.rechercher_prenom(prenom, csvSource=resultatNom)

    def rechercher_heure(self, fieldValue, csvSource=None):
        return self.rechercher(fieldValue, fieldName=self.c.HEURE, csvSource=csvSource)

    def rechercher_date_et_heure(self, date, heure):
        resultatDate = self.rechercher_date(date)
        return self.rechercher_heure(heure, csvSource=resultatDate)

    def rechercher_date_et_periode(self, date, finPeriode, dureePeriode):
        # FIXME add datetime to time check
        resultatDate = self.rechercher(date, fieldName=self.c.DATE)
        resultatPeriode = []
        for resultat in resultatDate:
            if resultat[self.c.HEURE] >= (finPeriode - dureePeriode):
                resultatPeriode.append(resultat)

        return resultatPeriode

    def verifier_cotisation(self, adherent):
        print("Entrees - verifier_cotisation: {}".format(adherent[self.c.DATE_FIN]))
        dateFin = self.convert_str_to_date(adherent[self.c.DATE_FIN])
        joursRestants = self.obtenir_date_actuelle() - dateFin
        print("JOURS RESTANTS!!!!", joursRestants.days)
        return - joursRestants.days

    # @profile
    def ecrire_entree(self, adherent):
        adherent[self.c.DATE] = str(self.obtenir_date_actuelle().strftime("%Y-%m-%d"))
        adherent[self.c.HEURE] = str(self.obtenir_heure_actuelle())
        if adherent[self.c.DATE_FIN] is not "visiteur":
            joursRestants = self.verifier_cotisation(adherent)
        else:
            joursRestants = False

        dejaScanne = self.detecter_deja_scanne(adherent)
        print("Entrees - ecrire_entree - dejaScanne: {}".format(dejaScanne))

        if not dejaScanne:
            adherent = {k: v for k, v in adherent.items() if k in self.fieldnames}
            self.csvDict.append(adherent)
            self.ajouter_ligne_csv(adherent, reload=False)

        self.update_stats()
        self.update_unique_stats()

        return {self.c.DEJA_SCANNE: dejaScanne, self.c.JOURS_RESTANTS: joursRestants}

    def ecrire_entree_visiteur(self, adherent):
        visiteur = {self.c.DATE_FIN: "visiteur"}
        visiteur.update(adherent)
        print("ecrire_entree_visiteur", visiteur)

        return self.ecrire_entree(visiteur)

    def rechercher_entrees(self, nom=None, prenom=None, jour=None):
        if nom is None and prenom is None and jour is None:
            raise UserWarning("Aucune clé n'a été fourni pour la recherche...")

        resultat = self.csvDict
        if nom is not None:
            resultat = self.rechercher_nom(nom, csvSource=resultat)

        if prenom is not None:
            resultat = self.rechercher_nom(prenom, csvSource=resultat)

        if jour is not None:
            resultat = self.rechercher_date(jour, csvSource=resultat)

        print("Entree - Rechercher: {}".format(resultat))

        return resultat

    def rechercher_entrees_du_jour(self, jour=None):
        if jour is None:
            jour = self.obtenir_date_actuelle()
        return self.rechercher_entrees(jour=jour)

    def detecter_deja_scanne(self, adherent):
        resultat = self.rechercher_entrees_du_jour()
        resultat = self.rechercher_heures(
            self.obtenir_heure_actuelle(), timedelta(0, 60 * 60 * 3), gt=False, csvSource=resultat)

        print("Entrees - detecter_deja_scanne 1: {}".format(resultat))
        resultat = self.rechercher_nom(adherent[self.c.NOM], csvSource=resultat)
        if not resultat:
            return False
        print("Entrees - detecter_deja_scanne 2: {}".format(resultat))
        resultat = self.rechercher_prenom(adherent[self.c.PRENOM], csvSource=resultat)
        if not resultat:
            return False
        else:
            return True

    # @profile
    def update_stats(self):
        ceJour = self.obtenir_date_actuelle()
        visiteurCeJour, visiteurCetteSemaine, visiteurCeMois = 0, 0, 0
        for ligne in self.csvDict:
            date = self.convert_str_to_date(ligne[self.c.DATE])
            if (ceJour - date).days < 1:
                visiteurCeJour += 1
                visiteurCetteSemaine += 1
                visiteurCeMois += 1
            elif (ceJour - date).days < 8:
                visiteurCetteSemaine += 1
                visiteurCeMois += 1
            elif (ceJour - date).days < 31:
                visiteurCeMois += 1

        self.stats = (visiteurCeJour, visiteurCetteSemaine, visiteurCeMois)
        return self.stats

    def entrees_unique_par_periode(self, nombreJour, uniques):
        entrees = self.rechercher_dates(self.obtenir_date_actuelle(), timedelta(nombreJour), self.c.DATE, gt=False)
        visiteursUnique = 0
        for unique in uniques:
            for ligne in entrees:
                if (ligne[self.c.NOM], ligne[self.c.PRENOM]) == unique:
                    visiteursUnique += 1
                    break
        return visiteursUnique

    # @profile
    def update_unique_stats(self):
        uniques = set([(l[self.c.NOM], l[self.c.PRENOM]) for l in self.csvDict])
        visiteurUniqueCeJour = self.entrees_unique_par_periode(1, uniques)
        visiteurUniqueCetteSemaine = self.entrees_unique_par_periode(7, uniques)
        visiteurUniqueCeMois = self.entrees_unique_par_periode(31, uniques)

        self.unique_stats = (visiteurUniqueCeJour, visiteurUniqueCetteSemaine, visiteurUniqueCeMois)
        return self.unique_stats
