from .ObjetCSV import ObjetCSV


class Bugs(ObjetCSV):
    """docstring for Bugs."""
    def __init__(self, *args, **kwargs):
        super(Bugs, self).__init__(*args, **kwargs)

    def ajouter_bug(self, ligneCsv):
        date = self.obtenir_date_actuelle()
        ligneCsv[self.c.DATE] = date
        self.csvDict.append(ligneCsv)
        self.write_csv()

    def obtenir_bugs(self):
        return self.csvDict
