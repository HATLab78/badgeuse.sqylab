import logging

from .Adherents import Adherents
from .ObjetCSV import ObjetCSV


logging.basicConfig(debug=logging.INFO)


class Badges(ObjetCSV):
    """docstring for Badge."""
    def __init__(self, *args, **kwargs):
        super(Badges, self).__init__(*args, **kwargs)

    def validate_csv(self):
        pass

    def validate_entetes_csv(self):
        assert self.c.RFID in self.fieldnames, "Entête manquante: {}".format(self.c.RFID)
        assert self.c.EMAIL in self.fieldnames, "Entête manquante: {}".format(self.c.EMAIL)
        logging.info("Entêtes csv OK.")

    def rechercher_rfid(self, rfid):
        return self.rechercher(rfid, fieldName=self.c.RFID)

    def rechercher_email(self, email):
        return self.rechercher(email, fieldName=self.c.EMAIL)

    def rechercher_numero_rfid_adherent(self, email):
        rfidInfo = self.rechercher_email(email)
        if not rfidInfo:
            rfid = None
        else:
            assert len(rfidInfo) == 1, "Plusieurs RFID pour l'adresse email: {}".format(email)
            rfid = rfidInfo[0][self.c.RFID]
        return rfid

    def supprimer_rfid(self, rfid):
        for ligne in self.csvDict:
            if ligne[self.c.RFID] == rfid:
                texte = "Vous avez supprimé l'ID de l'adhérent : {}".format(ligne[self.c.EMAIL])
                ligne[self.c.RFID] = ""
                break
        else:
            texte = "Pas d'adhérent associé à cet ID"

        self.write_csv()

        return texte

    def ajouter_rfid_adherent(self, adherent, rfid):
        """Associe un adhérent à un ID rfid."""
        if not rfid:
            return "Commencé par scanner un badge RFID"

        email = adherent[self.c.EMAIL]
        autreAdherent = self.rechercher_rfid(rfid)
        if autreAdherent:
            texte = "Numéro RFID déjà associer: {} à {}\nMerci de bien vouloir rapporter le bug."
            return texte.format(rfid, autreAdherent[0][self.c.EMAIL])

        if adherent[self.c.RFID]:
            return "Email déjà associé à un badge: {}\nSupprimer d'abord le badge.".format(adherent)
        else:
            self.csvDict = [r for r in self.csvDict if r[self.c.EMAIL] != adherent[self.c.EMAIL]]
            self.csvDict.append({self.c.EMAIL: email, self.c.RFID: rfid})
            self.write_csv()
            return "RFID: {} associé à: {}".format(rfid, email)

    # def lire_dernier_rfid(self):
    #     """Permet de lire le dernier badge non repertorié qui a été badgé."""
    #     with open(self.c.CHEMIN_TXT_DERNIER_BADGE, mode='r') as fichierBadge:
    #         contenu = fichierBadge.read()
    #
    #     return contenu.strip()
