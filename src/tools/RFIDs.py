import logging

from .ObjetCSV import ObjetCSV

# from datetime import timedelta


# from .utils.Profiler import profile
# from .utils.Validate import Validate


logging.basicConfig(level=logging.INFO)


class RFIDs(ObjetCSV):
    """Entrees permet de gérer le fichier adhérents et de le tester."""
    def __init__(self, *args, **kwargs):
        super(RFIDs, self).__init__(*args, **kwargs)

    def rechercher_formation(self):
        pass

    def ajouter_usage_machine(self):
        pass
