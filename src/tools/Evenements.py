from .ObjetCSV import ObjetCSV


class Evenements(ObjetCSV):
    """docstring for Evenements."""
    def __init__(self, *args, **kwargs):
        super(Evenements, self).__init__(*args, **kwargs)

    def ajouter_evenement(self, evenement):
        """Ajoute un evenement."""
        self.csvDict.append(evenement)
        self.write_csv()

    def obtenir_derniers_evenements(self, nombre):
        return self.csvDict[-nombre:]

    def editer_evenement(self, nom, date, participants):
        for ligne in self.csvDict:
            if nom == ligne[self.c.EVENEMENT] and date == ligne[self.c.DATE]:
                ligne[self.c.PARTICIPANTS] = participants
        self.write_csv()
