import logging

logging.basicConfig(filename='Web_app.log', level=logging.DEBUG)


def log_request(pageName, request):
    # logging.info("Page: {}".format(APP_ACCUEIL))
    method, form, args = request.method, str(request.form), str(request.args)
    logString = "Page: {}\n{} requête reçue".format(pageName, method)
    if request.method:
        logString += ":\nForm: {}\nArgs: {}".format(form, args)
    else:
        logString += ":\nNo Request"
    logging.info(logString)
