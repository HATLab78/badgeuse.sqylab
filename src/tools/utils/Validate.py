from datetime import date
from re import compile as re_compile

DAY = "day"
MONTH = "month"
YEAR = "year"
HEURE = "hr"
MINUTES = "min"
MME_MR_REGEX = re_compile(r"([M.|Mme])")
NOM_REGEX = re_compile(r"(\S+)")
EMAIL_REGEX = re_compile(r"(\S+)(@)(\S+)")
DATE_REGEX = re_compile(r"\d{2}\/\d{2}\/\d{2,4}")
RFID_REGEX = re_compile(r"\d+")
DATE_YEAR_REGEX = re_compile(r"(?P<{year}>\d{{4}})\W+(?P<{month}>\d{{2}})\W+(?P<{day}>\d{{2}}$)".format(
    day=DAY, month=MONTH, year=YEAR))
DATE_MONTH_REGEX = re_compile(r"(?P<{day}>\d{{2}})\W+(?P<{month}>\d{{2}})\W+(?P<{year}>\d{{4}}$)".format(
    day=DAY, month=MONTH, year=YEAR))
DATE_TIME_REGEX = re_compile(r"(?P<{HEURE}>^\d+)\D+(?P<{minutes}>\d+$)".format(HEURE=HEURE, minutes=MINUTES))
EMAIL_REGEX = re_compile(r"([\w\W]+?@[\w]+?\..+$)")
RFID_REGEX = re_compile(r"(\d{7})")


class Validate(object):
    """Objet pour gérer les dates."""
    def __init__(self, field):
        # super(DateObject, self).__init__()
        self.field = field

    def tester_date(self):
        result = DATE_YEAR_REGEX.match(self.field)
        self.dateRegex = DATE_YEAR_REGEX
        if not result:
            result = DATE_MONTH_REGEX.match(self.field)
            self.dateRegex = DATE_MONTH_REGEX

        assert result, "L'expression régulière n'a rien trouvé dans: {}".format(self.field)
        assert len(result.group(YEAR)) == 4 and result.group(YEAR).isdigit()
        assert len(result.group(MONTH)) == 2 and result.group(MONTH).isdigit()
        assert len(result.group(DAY)) == 2 and result.group(DAY).isdigit()
        return self.creer_date(result)

    def tester_heure(self):
        result = DATE_TIME_REGEX.match(self.field)
        self.timeRegex = DATE_TIME_REGEX
        assert result, "L'expression régulière n'a pas trouver d'heure dans: {}".format(self.field)
        assert len(result.group(HEURE)) == 2 and result.group(HEURE).isdigit()
        assert len(result.group(MINUTES)) == 2 and result.group(MINUTES).isdigit()
        return self.creer_heure(result)

    def creer_date(self, parsedDate):
        if not str(type(parsedDate)) == "<class '_sre.SRE_Match'>":
            raise AssertionError("Paramètre d'entrée de type incorrect: {}".format(type(parsedDate)))
        return date(int(parsedDate.group(YEAR)), int(parsedDate.group(MONTH)), int(parsedDate.group(DAY)))

    def creer_heure(self, heure):
        return heure[HEURE] + ":" + heure[MINUTES]

    def convertir_str_vers_date(self, dateString):
        return self.creer_date(self.dateRegex.match(dateString))

    def tester_email(self):
        result = EMAIL_REGEX.match(self.field)
        if result:
            return self.field

    def tester_rfid(self):
        result = RFID_REGEX.match(self.field)
        if result:
            return self.field
