import time

global funcMap
funcMap = {}


def profile(func):
    def wrap(*args, **kwargs):
        global funcMap
        funcMap.setdefault(func.__name__, [])
        started_at = time.time()
        result = func(*args, **kwargs)
        funcMap[func.__name__].append(time.time() - started_at)
        return result

    return wrap
