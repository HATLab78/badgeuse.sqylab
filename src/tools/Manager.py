from werkzeug.utils import secure_filename

from .utils.Profiler import profile


class Manager(object):
    """Classe pour gérer les adhérents et notamment les entrées."""
    def __init__(self, entrees, adherents, badges, emails, formations, constants, bugs=None, evenements=None):
        self.entrees = entrees
        self.adherents = adherents
        self.badges = badges
        self.emails = emails
        self.formations = formations
        self.dernierRfid = None
        self.c = constants
        # self.bugs = bugs
        # self.evenements = evenements

    # @profile
    def ajouter_entree(self, adherent):
        return self.entrees.ecrire_entree(adherent)

    # @profile
    def ajouter_visiteur(self, adherent):
        return self.entrees.ecrire_entree_visiteur(adherent)

    # @profile
    def rechercher_adherent_par_rfid(self, rfid):
        # TODO a supprimer quand l'appli sera stable
        self.badges.load_csv()
        resultat = self.badges.rechercher_rfid(rfid)
        assert len(resultat) <= 1, "Badge RFID associé plus d'une fois: {}".format(resultat)
        if resultat:
            return self.adherents.rechercher_email(resultat[0][self.c.EMAIL])
        else:
            return None

    # @profile
    def ajouter_rfid_adherent(self, adherent, rfid):
        return self.badges.ajouter_rfid_adherent(adherent, rfid)

    # @profile
    def ajouter_email(self, adherent):
        if not adherent or self.c.EMAIL not in adherent.keys():
            return False

        return self.emails.ajouter_email(adherent)

    # @profile
    def ajouter_evenement(self, evenement):
        return self.evenements.ajouter_evenement(evenement)

    # @profile
    def editer_evenement(self, evenement):
        return self.evenements.editer_evenement(evenement)

    # @profile
    def obtenir_derniers_evenements(self, nombreEvenements):
        return self.evenements.obtenir_derniers_evenements(nombreEvenements)

    # @profile
    def rechercher_entrees_du_jour(self):
        return self.entrees.rechercher_entrees_du_jour()

    # @profile
    def rechercher_entrees(self, nom=None, prenom=None, jour=None):
        return self.entrees.rechercher_entrees(nom=nom, prenom=prenom, jour=jour)

    # @profile
    def rechercher_date(self, date):
        return self.entrees.rechercher_date(date)

    # @profile
    def rechercher_entrees_adherent(self, adherent):
        return self.entrees.rechercher_entrees(nom=adherent[self.c.NOM], prenom=adherent[self.c.PRENOM])

    # @profile
    def rechercher_rfid_from_email(self, email):
        return self.badges.rechercher_numero_rfid_adherent(email)

    # @profile
    def rechercher_adherent(self, nom):
        from string import Template
        adherents = self.adherents.rechercher_adherent(nom=nom, prenom=nom)

        index = 0
        for adherent in adherents:
            adherent[self.c.RFID] = self.rechercher_rfid_from_email(adherent[self.c.EMAIL])
            uri = "/{{URI}}?{INDEX}=${INDEX}".format(INDEX=self.c.WEB_ADHERENT_INDEX)
            uriTemplate = Template(uri)
            adherent[self.c.URI] = uriTemplate.substitute({self.c.WEB_ADHERENT_INDEX: index})
            index += 1

        for adherent in adherents:
            print("Manager - Rechercher_adherent: {}".format(adherent))
        return adherents

    # @profile
    def lire_dernier_rfid(self):
        return self.badges.lire_dernier_rfid()

    # @profile
    def ajouter_bug(self, bug):
        return self.bugs.ajouter_bug(bug)

    # @profile
    def obtenir_bugs(self, numberOfBugs=10):
        return self.bugs.obtenir_bugs()

    # @profile
    def allowed_file(self, file):
        return self.adherents.allowed_file(file)

    # @profile
    def secure_filename(self, file):
        return secure_filename(file)

    # @profile
    def validate_fichier_adherent(self, file):
        return self.adherents.validate(file)

    # @profile
    def reecrire_registre_des_entrees(self):
        return self.adherents.reecrire_fichier()

    # @profile
    def supprimer_rfid_adherent(self, rfid):
        return self.badges.supprimer_rfid(rfid)

    def a_recu_formation(self, adherent, machine):
        return self.formations.a_recu_formation(adherent, machine)

    def verifier_cotisation(self, adherent):
        return self.entrees.verifier_cotisation(adherent)

    def colorer_entrees_avec_fin_cotisation(self, entrees):
        for entree in entrees:
            if entree[self.c.DATE_FIN] == self.c.WEB_VISITEUR:
                entree[self.c.COULEUR] = self.c.WEB_VISITEUR
                continue

            joursRestants = self.verifier_cotisation(entree)
            if joursRestants >= self.c.JOURS_RAPPEL:
                couleur = self.c.CAT_OK
            elif joursRestants <= self.c.JOURS_RAPPEL and joursRestants > 0:
                couleur = self.c.CAT_ATTENTION
            else:
                couleur = self.c.CAT_ERREUR
            entree[self.c.COULEUR] = couleur

        return entrees
