from os import path
from re import compile as re_compile


class Constants(object):
    """Classe pour les constantes."""
    # Constantes pour les Adhérents
    GENRE = "Genre"
    NOM = "NOM"
    PRENOM = "Prenom"  # avec ou sans accent?
    EMAIL = "E-mail"
    EMAIL_TYPE = "email"
    PASSWORD = "password"
    DATE = "Date"
    DATE_FIN = "DateFinAdhesion"
    DATE_FORMAT = "%Y-%m-%d"
    DATE_FORMAT_ALTERNATIF = "%Y/%m/%d"
    HEURE_FORMAT = "%H:%M"
    HEURE = "Heure"
    RFID = "rfid"
    RFID_NON_ATTRIBUE = "rfidNonAttribue"
    URI = "uri"
    COTISATION = "cotisation"
    DEJA_SCANNE = "deja_scanne"
    CAT_OK = "ok"
    CAT_ATTENTION = "attention"
    CAT_ERREUR = "erreur"
    COULEUR = "couleur"
    JOURS_RESTANTS = "jours_restants"
    JOURS_RAPPEL = 15
    # Constantes pour les Évènements
    EVENEMENT = "evenement"
    PARTICIPANTS = "participants"

    ALLOWED_EXTENSIONS = set(["csv", "txt"])

    # Constantes des actions web
    WEB_ENVOYER = "envoyer"
    WEB_SUPPRIMER = "supprimer"
    WEB_ENTREE = "entree"
    WEB_TELEVERSER = "televerser"
    WEB_AJOUTER = "ajouter"
    WEB_ACTION = "action"
    WEB_DATA = "data"
    WEB_ACTIVE = "active"
    WEB_DERNIER = "dernier"
    WEB_VISITEUR = "visiteur"
    WEB_ORGANISME = "organisme"
    WEB_POST = "POST"
    WEB_GET = "GET"
    WEB_PUT = "PUT"
    WEB_BOUTON = "bouton"
    WEB_NUMERO = "numero"
    WEB_TEXTE = "texte"
    WEB_CONTENU = "contenu"
    WEB_ADHERENT = "adherent"
    WEB_ADHERENTS = "adherents"
    WEB_ADHERENT_INDEX = "index"
    WEB_RECHERCHER = "rechercher"
    WEB_ENTREES = "entrees"
    WEB_JOUR = "jour"
    WEB_MOIS = "mois"
    WEB_ANNEE = "annee"
    WEB_DATE = "date"

    CHEMIN_CHANGELOG = "CHANGELOG.md"

    HTML_WRAPPER = [
        "primary",
        "secondary",
        "success",
        "danger",
        "warning",
        "info",
        "dark"
    ]
    HTML_FLASH = '<div class="temp alert lead alert-{} .alert-dismissible" role="alert">{}</div>'
    STREAM_TYPE = re_compile(r"<(\w+)>(.+)")
    BOUTON_AJOUT_BADGE = """
<a class="btn btn-primary " name="bouton" value="ajouter" href="{}" role="button">Associer</a>
    """

    # TELEGRAM_API_URL = "https://api.telegram.org/bot{}/sendMessage".format(TELEGRAM_API_TOKEN)
    # TELEGRAM_API_MESSAGE_PAYLOAD = {"chat_id": TELEGRAM_API_CHAT_ID, "text": "HELLO FROM PYTHON"}
    DESCRIPTION = "description"

    # Upload
    UPLOAD_FOLDER = "uploads"

    # RFID
    RFID = "rfid"
    CHEMIN_TXT_DERNIER_BADGE = "data/dernier_badge_scanne.txt"
    MACHINES = ["laser", "i3d", "cnc"]

    def __init__(self, dataPath="data"):
        super(Constants, self).__init__()
        # Constantes des urls
        self.APP_ROOT = ""
        self.APP_ACCUEIL = "accueil"
        self.APP_HISTORIQUE = "historique"
        self.APP_ADMIN = "admin"
        self.APP_LOGIN = "login"
        self.APP_LOGOUT = "logout"
        self.APP_REGISTER = "register"
        self.APP_VISITEUR = "visiteur"
        self.APP_BUG = "bug"
        self.APP_NEWS = "newsletter"
        self.APP_ADHESION = "adhesion"
        self.APP_CHANGELOG = "changelog"
        self.APP_EVENEMENT = "evenement"
        self.APP_BUGS = "bugs"
        self.APP_EMAIL = "etienne.pouget@outlook.com"
        self.APP_STREAM = "stream"
        self.APP_SIMULER = "simuler"
        self.APP_RECHERCHE = "recherche"
        self.APP_VISITEUR = "visiteur"
        self.APP_PATHS = self.generer_chemins()
            # Chemin des Fichiers
        self.DATA_FOLDER = path.abspath(path.join(path.curdir, dataPath))
        self.ADHERENTS_CSV = path.join(self.DATA_FOLDER, "adherents.csv")
        self.ENTREES_CSV = path.join(self.DATA_FOLDER, "entrees.csv")
        self.BADGE_CSV = path.join(self.DATA_FOLDER, "badges.csv")
        self.EVENEMENT_CSV = path.join(self.DATA_FOLDER, "evenements.csv")
        self.BUGS_CSV = path.join(self.DATA_FOLDER, "bugs.csv")
        self.VISITEURS_CSV = path.join(self.DATA_FOLDER, "visiteurs.csv")
        self.FORMATIONS_CSV = path.join(self.DATA_FOLDER, "formations.csv")
        self.CHEMIN_SCRIPT_DEMARRER_DATABASE = path.abspath(path.join(path.curdir, "src/sql/start.sql"))
        self.CHEMIN_DATABASE = path.abspath(path.join(self.DATA_FOLDER, "admin.db"))

    def generer_chemins(self):
        PATHS = {}
        for item in self.__dict__.keys():
            if "APP_" in item:
                PATHS[getattr(self, item)] = "/" + getattr(self, item)

        return PATHS
